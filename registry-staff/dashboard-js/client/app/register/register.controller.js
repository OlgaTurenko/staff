'use strict';

angular.module('dashboardJsApp')
  .controller('RegisterCtrl', function(Reg, Idle, Modal, $scope, $location) {


    $scope.userReg = {/*sFirstName : 'Туренко' ,
                      sLastName  : 'Сергей'  ,
                      sMiddleName: 'Валериевич',
                      sLogin     : 'turenko'     ,
                      sPassword  : 'turenko'  ,
                      sPhone     : 123456789     ,
                      sEmail     : 'asd@dsa'     ,
                      sPosition  : 'Президент'  ,
                      sOrgan     : 'admin'    */ };
    $scope.errors  = {};

    //$scope.organs    = Reg.getOrgans   ($scope);
    $scope.groups    = Reg.getGroups   ($scope);
    //$scope.positions = Reg.getPositions($scope);

    $scope.registerUser = function(form) {
      if( $scope.userReg.sPassword != $scope.password2) $scope.errors = "Паролі не співпадають";
      if (form.$valid && $scope.userReg.sPassword == $scope.password2) {
        $scope.submitted = true;
        Reg.register($scope);
      }
    };
    //var res = $http.post('/register', userReg);
    //res.success(function(data, status, headers, config) {
    //  $scope.message = data;
    //});
  }
);
