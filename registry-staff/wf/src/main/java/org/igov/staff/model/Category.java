package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;


@javax.persistence.Entity
public class Category extends NamedEntity implements WfClonable<Category>{
    @Transient
    @Override
    public Category clone() throws CloneNotSupportedException {
        return (Category)super.clone();
    }

}
