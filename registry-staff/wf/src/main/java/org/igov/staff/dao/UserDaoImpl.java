package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.igov.staff.model.User;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository 
public class UserDaoImpl extends GenericEntityDao<User> implements UserDao {

    private static final Logger log = Logger.getLogger(UserDaoImpl.class);

    protected UserDaoImpl() {
        super(User.class);
    }
    
    @Override
    public User getUserByLogin(String sLogin){
    Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("sLogin", sLogin));
        User organ = (User) criteria.uniqueResult();
        return organ;
    }
}
