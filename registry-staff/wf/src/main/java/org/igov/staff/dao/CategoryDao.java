package org.igov.staff.dao;

import org.igov.staff.model.Category;
import org.igov.model.core.EntityDao;

public interface CategoryDao extends EntityDao<Category> {

}

