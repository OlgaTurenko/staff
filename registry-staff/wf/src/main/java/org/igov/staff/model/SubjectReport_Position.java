package org.igov.staff.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.igov.model.core.Entity;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class SubjectReport_Position extends Entity implements WfClonable<SubjectReport_Position>, Serializable{
    
    @JsonIgnore
    @ManyToOne(targetEntity = SubjectReport_StructureOrgan.class, fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "nID_SubjectReport_StructureOrgan")
    private SubjectReport_StructureOrgan oSubjectReport_StructureOrgan;

    @JsonProperty(value = "oPosition")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Position", nullable = false)
    private Position oPosition;


    @JsonProperty(value = "aSubjectReport_Category")
    @OneToMany(mappedBy = "oSubjectReport_Position", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<SubjectReport_Category> aSubjectReport_Category = new ArrayList<>();

    @JsonProperty(value = "nCountHired_Re")
    @Column
    private Integer nCountHired_Re;

    @JsonProperty(value = "nCountHiredCompet_Re")
    @Column
    private Integer nCountHiredCompet_Re;

    public SubjectReport_StructureOrgan getoSubjectReport_StructureOrgan() {
        return oSubjectReport_StructureOrgan;
    }

    public void setoSubjectReport_StructureOrgan(SubjectReport_StructureOrgan oSubjectReport_StructureOrgan) {
        this.oSubjectReport_StructureOrgan = oSubjectReport_StructureOrgan;
    }

    public Position getoPosition() {
        return oPosition;
    }

    public void setoPosition(Position oPosition) {
        this.oPosition = oPosition;
    }

    public Integer getnCountHired_Re() {
        return nCountHired_Re;
    }

    public void setnCountHired_Re(Integer nCountHired_Re) {
        this.nCountHired_Re = nCountHired_Re;
    }

    public Integer getnCountHiredCompet_Re() {
        return nCountHiredCompet_Re;
    }

    public void setnCountHiredCompet_Re(Integer nCountHiredCompet_Re) {
        this.nCountHiredCompet_Re = nCountHiredCompet_Re;
    }

    public List<SubjectReport_Category> getaSubjectReport_Category() {
        return aSubjectReport_Category;
    }

    public void setaSubjectReport_Category(
            List<SubjectReport_Category> aSubjectReport_Category) {
        this.aSubjectReport_Category = aSubjectReport_Category;
    }
    @Transient
    @Override
    public SubjectReport_Position clone() throws CloneNotSupportedException {
        return (SubjectReport_Position) super.clone();
    }
    @JsonIgnore
    @Transient
    public SubjectReport_Position getCopyWithoutStructureCategories() throws CloneNotSupportedException {
        SubjectReport_Position o = this.clone();
        o.setaSubjectReport_Category(null);
        return o;
    }

}
