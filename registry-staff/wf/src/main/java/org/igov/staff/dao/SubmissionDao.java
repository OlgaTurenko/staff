package org.igov.staff.dao;


import org.igov.staff.model.Submission;
import org.igov.model.core.EntityDao;

public interface SubmissionDao extends EntityDao<Submission> {

}

