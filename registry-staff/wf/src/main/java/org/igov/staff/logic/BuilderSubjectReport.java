package org.igov.staff.logic;

import javassist.NotFoundException;
import org.igov.staff.dao.SubjectReport_CategoryDao;
import org.igov.staff.dao.SubjectReport_PositionDao;
import org.igov.staff.dao.SubjectReport_StructureOrganDao;
import org.igov.staff.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sergey_PC on 04.11.2015.
 */

@Component
public class BuilderSubjectReport {
    private List<SubjectReport_Position> subjectReportPositions ;
    private List<SubjectReport_Category> subjectReportCategories;
    @Autowired
    private SubjectReport_StructureOrganDao reportStructureOrganDao;
    @Autowired
    private SubjectReport_PositionDao reportPositionDao;
    @Autowired
    private SubjectReport_CategoryDao reportCategoryDao;

    public SubjectReport_StructureOrgan createInstance(StructureOrgan structureOrgan, Period period, List<Position> positions, List<Category> categories) throws NotFoundException{
        if(structureOrgan == null) throw new NotFoundException("[structureOrgan] is empty");
        if(period         == null) throw new NotFoundException("[period] is empty"        );
        if(positions      == null) throw new NotFoundException("[positions] is empty"     );
        if(categories     == null) throw new NotFoundException("[categories] is empty"    );
        subjectReportPositions = new ArrayList<>();
        subjectReportCategories= new ArrayList<>();
        SubjectReport_StructureOrgan subjectReportStructureOrgan;
        subjectReportStructureOrgan = new SubjectReport_StructureOrgan();
        subjectReportStructureOrgan.setoPeriod(period);
        subjectReportStructureOrgan.setoStructureOrgan(structureOrgan);
        initAndStoreSubjectReportCategoryList(categories);
        initAndStoreSubjectReportPositionList(positions);
        subjectReportStructureOrgan.setaSubjectReport_Position((List) ((ArrayList) subjectReportPositions).clone());
        return subjectReportStructureOrgan;
    }

    protected void initAndStoreSubjectReportPositionList(List<Position> positions){
        SubjectReport_Position position = null;
        for(int i=0; i<positions.size(); i++){
            position = new SubjectReport_Position();
            position.setoPosition(positions.get(i));
            position.setaSubjectReport_Category((List)((ArrayList) subjectReportCategories).clone());
            subjectReportPositions.add(position);
        }
    }

    protected void initAndStoreSubjectReportCategoryList(List<Category> categories){
        SubjectReport_Category ctg = null;
        if(categories!= null)
            for(int i=0; i<categories.size(); i++){
                ctg = new SubjectReport_Category();
                ctg.setoCategory(categories.get(i));
                subjectReportCategories.add(ctg);
            }
    }

    public SubjectReport_StructureOrgan createAndStoreInstance(StructureOrgan structureOrgan, Period period, List<Position> positions, List<Category> categories) throws NotFoundException{
        if(structureOrgan == null) throw new NotFoundException("[structureOrgan] is empty");
        if(period         == null) throw new NotFoundException("[period] is empty"        );
        if(positions      == null) throw new NotFoundException("[positions] is empty"     );
        if(categories     == null) throw new NotFoundException("[categories] is empty"    );

        SubjectReport_StructureOrgan subjectReportStructureOrgan;
        subjectReportPositions  = new ArrayList<>();
        subjectReportCategories = new ArrayList<>();
        subjectReportStructureOrgan = new SubjectReport_StructureOrgan();
        try {
            subjectReportStructureOrgan.setoPeriod(period);
            subjectReportStructureOrgan.setoStructureOrgan(structureOrgan.getCopyWithoutReportStructureOrgan());
            subjectReportStructureOrgan = reportStructureOrganDao.saveOrUpdate(subjectReportStructureOrgan);
            initAndStoreSubjectReportPositionList(subjectReportStructureOrgan, positions, categories);
            subjectReportStructureOrgan.setaSubjectReport_Position((List) ((ArrayList) subjectReportPositions).clone());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return subjectReportStructureOrgan;
    }

    protected void initAndStoreSubjectReportPositionList(SubjectReport_StructureOrgan subjectReportStructureOrgan, List<Position> positions, List<Category> categories) throws CloneNotSupportedException {
        SubjectReport_Position position = null;
        for(int i=0; i<positions.size(); i++){
            position = new SubjectReport_Position();
            position.setoPosition(positions.get(i));
            position.setoSubjectReport_StructureOrgan(subjectReportStructureOrgan.getCopyWithoutStructurePositions());
            subjectReportPositions.add(position);
            position = reportPositionDao.saveOrUpdate(position);
            initAndStoreSubjectReportCategoryList(position, categories);
            subjectReportStructureOrgan.getaSubjectReport_Position().add(position);
        }
    }

    protected void initAndStoreSubjectReportCategoryList(SubjectReport_Position subjectReportPosition, List<Category> categories) throws CloneNotSupportedException {
        SubjectReport_Category ctg = null;
        if(categories!= null)
            for(int i=0; i<categories.size(); i++){
                ctg = new SubjectReport_Category();
                ctg.setoCategory(categories.get(i));
                ctg.setoSubjectReport_Position(subjectReportPosition.getCopyWithoutStructureCategories());
                reportCategoryDao.saveOrUpdate(ctg);
                subjectReportPosition.getaSubjectReport_Category().add(ctg);
            }
    }
}
