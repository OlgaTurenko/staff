package org.igov.staff.rest.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.igov.staff.activiti.logic.Identity;
import org.igov.staff.activiti.logic.IdentityInterface;
import org.igov.staff.dao.SubjectReport_CategoryDao;
import org.igov.staff.model.Organ;
import org.igov.staff.model.SubjectReport_Category;
import org.igov.staff.model.SubjectReport_Position;
import org.igov.staff.model.SubjectReport_StructureOrgan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author dmaidaniuk
 */
@Controller
@RequestMapping(value = "/staff")
public class ReportGenerationController {

    private static final Logger LOG = LoggerFactory.getLogger(ReportGenerationController.class);

    private static final String CELL_SEPARATOR = ";";

    private static final String WRONG_PARAMS_ERROR = "Wrong arguments used. yearFrom, yearTo, quaterFrom and quaterTo should be bigger than 0.";
    
    private static final String WRONG_PARAM_ORGAN_ERROR = "Wrong argument used. organName should be presented.";

    private static final String[] TITLES = {
        "Рік",
        "Квартал",
        "Код державного органу",
        "Назва державного органу",
        "Гілка влади", 
        "Підпорядкування",
        "Тип1 державного органу",
        "Тип2 державного органу",
        "Складова структури органу виконавчої влади",
        "Посада",
        "Категорія",
        //"Кількість структурних підрозділів / територіальних органів у складі державного органу",
        //"Кількість державних службовців, призначених з початку року, якi вже перебували на державній службі",
        //"Кількість державних службовців, призначених з початку року на конкурсній основі, якi вже перебували на державній службі",
        "Кількість посад державних службовців за штатним розписом",
        "Облікова кількість державних службовців",
        "Кількість вакантних посад",
        "Кількість державних службовців, призначених з початку року",
        "Кількість державних службовців, призначених з початку року на конкурсній основі",
        "Кількість державних службовців, звільнених з початку року"
    };

    @Autowired
    private SubjectReport_CategoryDao dao;

    @Autowired
    private IdentityInterface identityInterface;

    @RequestMapping(value = "/generateReportInXls", method = RequestMethod.GET, produces = "application/vnd.ms-excel")
    public void generateXls(@RequestParam(value = "yearFrom", required = true) Integer nYearFrom,
            @RequestParam(value = "yearTo", required = true) Integer nYearTo,
            @RequestParam(value = "quaterFrom", required = true) Integer nQuaterFrom,
            @RequestParam(value = "quaterTo", required = true) Integer nQuaterTo,
            @RequestParam(value = "organName", required = false) String sOrganName,
            HttpServletResponse response) throws IOException {

        if (nYearFrom <= 0 || nYearTo <= 0 || nQuaterFrom <= 0 || nQuaterTo <= 0) {
            throw new IllegalStateException(WRONG_PARAMS_ERROR);
        }

        boolean hasAdminRights = identityInterface.hasRights(sOrganName, Identity.Role.SECURITY);

        if (!hasAdminRights && sOrganName == null) {
            throw new IllegalStateException(WRONG_PARAM_ORGAN_ERROR);
        }
        else if (hasAdminRights) {
            sOrganName = null; // clear value
        }
        
        response.setContentType("application/vnd.ms-excel; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        String reportName = "Report_from:" + nYearFrom + "_" + nQuaterFrom + "_to:" + nYearTo + "_" + nQuaterTo + ".xlsx";
        response.setHeader("Content-Disposition", "attachment; filename=" + reportName);

        XSSFWorkbook workbook = new XSSFWorkbook();
        Map<String, CellStyle> styles = createStyles(workbook);
        XSSFSheet sheet = workbook.createSheet("Звіт");
        setSheetStyle(sheet);

        LOG.debug("write titles...");
        writeDataToExcelRow(sheet, 1, TITLES, styles);
        sheet.createFreezePane(5, 2);
        LOG.debug("write titles ok!");

        LOG.debug("get rows data...");
        Collection<ReportEntry> dataRows = generateDataRows(nYearFrom, nYearTo, nQuaterFrom, nQuaterTo, sOrganName);
        LOG.debug("get rows data ok!");

        LOG.debug("write rows data...");
        writeDataToExcelRows(sheet, 2, dataRows, styles);
        LOG.debug("write rows data ok! send report...");
        
        workbook.write(response.getOutputStream());
    }

    /**
     * create a library of cell styles
     */
    public static Map<String, CellStyle> createStyles(Workbook wb) {
        Map<String, CellStyle> styles = new HashMap<>();

        CellStyle style;
        Font headerFont = wb.createFont();
        headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFont(headerFont);
        style.setWrapText(true);
        styles.put("header", style);

        Font font1 = wb.createFont();
        font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
        style = createBorderedStyle(wb);
        style.setAlignment(CellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        style.setFont(font1);
        style.setWrapText(true);
        styles.put("cell_h", style);

        return styles;
    }

    private static CellStyle createBorderedStyle(Workbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setBorderRight(CellStyle.BORDER_THIN);
        style.setRightBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderBottom(CellStyle.BORDER_THIN);
        style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderLeft(CellStyle.BORDER_THIN);
        style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
        style.setBorderTop(CellStyle.BORDER_THIN);
        style.setTopBorderColor(IndexedColors.BLACK.getIndex());
        return style;
    }

    private static void setSheetStyle(XSSFSheet sheet) {
        //turn off gridlines
        sheet.setDisplayGridlines(false);
        sheet.setPrintGridlines(false);
        sheet.setFitToPage(true);
        sheet.setHorizontallyCenter(true);
        PrintSetup printSetup = sheet.getPrintSetup();
        printSetup.setLandscape(true);

        //group rows for each phase, row numbers are 0-based
        sheet.groupRow(4, 6);
        sheet.groupRow(9, 13);
        sheet.groupRow(16, 18);

        //set column widths, the width is measured in units of 1/256th of a character width
        sheet.setColumnWidth(0, 256 * 13);
        sheet.setColumnWidth(3, 256 * 33);
        sheet.setColumnWidth(4, 256 * 33);
        sheet.setColumnWidth(5, 256 * 33);
        sheet.setColumnWidth(6, 256 * 33);
        sheet.setColumnWidth(7, 256 * 33);
        sheet.setColumnWidth(8, 256 * 33);
        sheet.setColumnWidth(9, 256 * 33);
        sheet.setColumnWidth(10, 256 * 33);
        sheet.setColumnWidth(11, 256 * 10);
    }

    @RequestMapping(value = "/generateReportInCsv", method = RequestMethod.GET, produces = "text/csv")
    public void generateCsv(@RequestParam(value = "yearFrom", required = true) Integer nYearFrom,
            @RequestParam(value = "yearTo", required = true) Integer nYearTo,
            @RequestParam(value = "quaterFrom", required = true) Integer nQuaterFrom,
            @RequestParam(value = "quaterTo", required = true) Integer nQuaterTo,
            HttpServletResponse response) throws IOException {

        if (nYearFrom <= 0 || nYearTo <= 0 || nQuaterFrom <= 0 || nQuaterTo <= 0) {
            throw new IllegalStateException(WRONG_PARAMS_ERROR);
        }

        response.setContentType("text/csv; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        String reportName = "Report_from:" + nYearFrom + "_" + nQuaterFrom + "_to:" + nYearTo + "_" + nQuaterTo + ":.csv";
        response.setHeader("Content-Disposition", "attachment; filename=" + reportName);

        LOG.debug("write titles...");
        String csvTitleRow = "";
        for (String title : TITLES) {
            csvTitleRow += title + CELL_SEPARATOR;
        }
        csvTitleRow += IOUtils.LINE_SEPARATOR;

        StringBuilder reportContent = new StringBuilder(csvTitleRow);
        LOG.debug("write titles ok!");

        LOG.debug("get rows data...");
        Collection<ReportEntry> dataRows = generateDataRows(nYearFrom, nYearTo, nQuaterFrom, nQuaterTo, null);
        LOG.debug("get rows data ok!");

        LOG.debug("write rows data...");
        Iterator<ReportEntry> iter = dataRows.iterator();
        while (iter.hasNext()) {
            ReportEntry entry = (ReportEntry) iter.next();
            reportContent.append(entry.toString());
            reportContent.append(IOUtils.LINE_SEPARATOR);
        }
        String content = reportContent.toString();
        response.getWriter().write(content);
        LOG.debug("write rows data ok! send report...");
    }

    private Collection<ReportEntry> generateDataRows(Integer nYearFrom, Integer nYearTo,
            Integer nQuaterFrom, Integer nQuaterTo, String sOrganName) {

        LOG.debug("getSubjectReport_CategoriesByYearAndQuater...");
        List<SubjectReport_Category> entities = dao.getSubjectReport_CategoriesByYearAndQuater(nYearFrom, nYearTo, 
                                                                                nQuaterFrom, nQuaterTo, sOrganName);
        LOG.debug("getSubjectReport_CategoriesByYearAndQuater ok!");

        Collection<ReportEntry> reportDatas = new ArrayList<>();
        for (SubjectReport_Category category : entities) {
            reportDatas.add(new ReportEntry(category));
        }

        return reportDatas;
    }

    private void writeDataToExcelRow(XSSFSheet sheet, int rownum, String[] values, Map<String, CellStyle> styles) {
        Row row = sheet.createRow(rownum);
        int cellnum = 1;
        for (String cellValue : values) {
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue(cellValue);
            cell.setCellStyle(styles.get("header"));
        }
    }

    private void writeDataToExcelRows(XSSFSheet sheet, int rownum, Collection<ReportEntry> entries, Map<String, CellStyle> styles) {
        int srartRowNum = rownum;
        for (ReportEntry entry : entries) {
            Row row = sheet.createRow(srartRowNum++);
            int cellnum = 1;
            for (Object cellValue : entry.getDataAsList()) {
                Cell cell = row.createCell(cellnum++);
                if (cellValue instanceof String) {
                    cell.setCellValue((String) cellValue);
                } else if (cellValue instanceof Integer) {
                    cell.setCellValue((Integer) cellValue);
                }
                cell.setCellStyle(styles.get("cell_h"));
            }

        }
    }

    private static class ReportEntry {

        String sOrganName;

        String sOrganNameShort;

        String sGovernanceName;

        String sSubmissionName;

        String sOrganTypeCommonName;

        String sOrganTypeDetailedName;

        String sStructureName;

        String sPositionName;

        String sCategoryName;

        Integer nPeriodYear;

        Integer nPeriodQuater;

        Integer nSubjectReportStructureOrganCountOrgan;

        Integer nSubjectReportPositionCountHired_Re;

        Integer nSubjectReportPositionCountHiredCompet_Re;

        Integer nSubjectReportCategoryCountHired;

        Integer nSubjectReportCategoryCountHiredCompet;

        Integer nSubjectReportCategoryCountFired;

        Integer nSubjectReportCategoryCountPlan;

        Integer nSubjectReportCategoryCountFact;

        Integer nSubjectReportCategoryCountVacancy;

        public ReportEntry(SubjectReport_Category source) {
            SubjectReport_StructureOrgan subjectReportStructureOrgan
                    = source.getoSubjectReport_Position().getoSubjectReport_StructureOrgan();
            Organ organ = subjectReportStructureOrgan.getoStructureOrgan().getoOrgan();
            SubjectReport_Position position = source.getoSubjectReport_Position();
            this.sOrganName = organ.getName();
            this.sOrganNameShort = organ.getsNameShort();
            this.sGovernanceName = organ.getoGovernance().getName();
            this.sSubmissionName = organ.getoSubmission().getName();
            this.sOrganTypeCommonName = organ.getoOrganTypeCommon().getName();
            this.sOrganTypeDetailedName = organ.getoOrganTypeDetailed().getName();
            this.sStructureName = subjectReportStructureOrgan.getoStructureOrgan().getoStructure().getName();
            this.sPositionName = source.getoSubjectReport_Position().getoPosition().getName();
            this.sCategoryName = source.getoCategory().getName();
            this.nPeriodYear = subjectReportStructureOrgan.getoPeriod().getnYear();
            this.nPeriodQuater = subjectReportStructureOrgan.getoPeriod().getnQuater();
            this.nSubjectReportStructureOrganCountOrgan = subjectReportStructureOrgan.getnCountOrgan();
            this.nSubjectReportPositionCountHired_Re = position.getnCountHired_Re();
            this.nSubjectReportPositionCountHiredCompet_Re = position.getnCountHiredCompet_Re();
            this.nSubjectReportCategoryCountHired = source.getnCountHired();
            this.nSubjectReportCategoryCountHiredCompet = source.getnCountHiredCompet();
            this.nSubjectReportCategoryCountFired = source.getnCountFired();
            this.nSubjectReportCategoryCountPlan = source.getnCountPlan();
            this.nSubjectReportCategoryCountFact = source.getnCountFact();
            this.nSubjectReportCategoryCountVacancy = source.getnCountVacancy();
        }

        public List<Object> getDataAsList() {
            List<Object> result = new LinkedList<>();
            result.add(nPeriodYear);
            result.add(nPeriodQuater);
            result.add(sOrganName);
            result.add(sOrganNameShort);
            result.add(sGovernanceName);
            result.add(sSubmissionName);
            result.add(sOrganTypeCommonName);
            result.add(sOrganTypeDetailedName);
            result.add(sStructureName);
            result.add(sPositionName);
            result.add(sCategoryName);
            //result.add(nSubjectReportStructureOrganCountOrgan);
            //result.add(nSubjectReportPositionCountHired_Re);
            //result.add(nSubjectReportPositionCountHiredCompet_Re);
            result.add(nSubjectReportCategoryCountPlan);
            result.add(nSubjectReportCategoryCountFact);
            result.add(nSubjectReportCategoryCountVacancy);
            result.add(nSubjectReportCategoryCountHired);
            result.add(nSubjectReportCategoryCountHiredCompet);
            result.add(nSubjectReportCategoryCountFired);
            return result;
        }

        @Override
        public String toString() {
            return StringUtils.join(getDataAsList(), CELL_SEPARATOR);
        }

    }

}
