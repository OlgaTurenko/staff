package org.igov.staff.activiti;

public enum ProccessBP {
    formTitle,
    formCountStaff,
    formCountHired,
    formCountFired;
}
