package org.igov.staff.activiti.task.listener;

import java.util.List;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.task.Task;
import org.igov.staff.activiti.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("assignTaskToUser")
public class AssignTaskToUser implements TaskListener {

    private static final Logger LOG = LoggerFactory.getLogger(AssignTaskToUser.class);

    @Override
    public void notify(DelegateTask delegateTask) {
        DelegateExecution execution = delegateTask.getExecution();
        String sDecision = (String) execution.getVariableLocal(Constant.sDecision);
        if ("save".equalsIgnoreCase(sDecision)) {
            List<Task> historicTasks = execution.getEngineServices().getTaskService()
                    .createTaskQuery()
                    .processInstanceId(execution.getProcessInstanceId())
                    .orderByTaskId().desc().list();
            if (historicTasks != null && !historicTasks.isEmpty()) {
                delegateTask.setAssignee(historicTasks.get(0).getAssignee());
                LOG.info("assign user " + historicTasks.get(0).getAssignee() + " ok!");
            }
        }
    }

}
