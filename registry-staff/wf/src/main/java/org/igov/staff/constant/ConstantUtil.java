package org.igov.staff.constant;

public class ConstantUtil {
    
    public static final String separator = ";";
    public static final String managerUserPrefix = "head";
    public static final String managedUserPrefix = "user";
}
