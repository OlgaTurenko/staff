package org.igov.staff.activiti.serviceTask;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javassist.NotFoundException;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.identity.Group;
import org.activiti.engine.runtime.ProcessInstance;
import org.igov.staff.activiti.Constant;
import org.igov.staff.activiti.ProccessBP;
import org.igov.staff.activiti.Util;
import org.igov.staff.dao.CategoryDao;
import org.igov.staff.dao.LanchBpDao;
import org.igov.staff.dao.OrganDao;
import org.igov.staff.dao.PeriodDao;
import org.igov.staff.dao.PositionDao;
import org.igov.staff.dao.StatusBpDao;
import org.igov.staff.dao.StructureOrganDao;
import org.igov.staff.dao.SubjectReport_StructureOrganDao;
import org.igov.staff.logic.BuilderSubjectReport;
import org.igov.staff.model.Category;
import org.igov.staff.model.LanchBP;
import org.igov.staff.model.Organ;
import org.igov.staff.model.Period;
import org.igov.staff.model.Position;
import org.igov.staff.model.StatusBP;
import org.igov.staff.model.Structure;
import org.igov.staff.model.StructureOrgan;
import org.igov.staff.model.SubjectReport_StructureOrgan;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("sendForm")
public class SendForm implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(SendForm.class);

    @Autowired
    private IdentityService identityService;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private StructureOrganDao structureOrganDao;

    @Autowired
    private PeriodDao periodDao;

    @Autowired
    private PositionDao positionDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private OrganDao organDao;

    @Autowired
    private SubjectReport_StructureOrganDao subjectReport_StructureOrganDao;

    @Autowired
    private LanchBpDao lanchBpDao;
    
    @Autowired
    private StatusBpDao statusBpDao;

    @Autowired
    private BuilderSubjectReport builderSubjectReport;

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        Integer nYear = Integer.parseInt((String) execution.getVariable(Constant.nYear));
        Integer nQuater = Integer.parseInt((String) execution.getVariable(Constant.nQuater));
        Period period = periodDao.getPeriod(nYear, nQuater);
        if (period == null) {
            throw new NotFoundException("period is not presented!");
        }
        StatusBP statusBP_beforeSendForm = statusBpDao.findById(Long.valueOf(1)).get();
        StatusBP statusBP_afterSendForm = statusBpDao.findById(Long.valueOf(2)).get();
        List<SubjectReport_StructureOrgan> subjectReport_StructureOrgans = subjectReport_StructureOrganDao.findAllBy("oPeriod.id", period.getId());
        if (subjectReport_StructureOrgans != null && subjectReport_StructureOrgans.size() > 0) {
            throw new NotFoundException("The forms already have been generated!");
        }
        
        //получения списка позиций
        List<Position> positions = positionDao.findAll();
        //получения списка категорий
        List<Category> categories = categoryDao.findAll();
        // получение списка групп
        List<Group> groups = identityService.createGroupQuery().list();
        for (Group group : groups) {
            //получение объекта сабджект_орган
            if (group != null && !Util.isItAdmin(group.getId())) { //при тестировании на локально машине убрать проверку ид-к
                Organ organ = organDao.getOrgan(group.getId());
                //получение объекта сабджект_орган
                if (organ != null) {
                    //if (organ.getName() != null && (organ.getName().startsWith("a") || organ.getName().startsWith("sales"))) {
                        List<StructureOrgan> aStructureOrgan = structureOrganDao.getaStructureOrganByID_Organ(organ.getId());
                        if (aStructureOrgan != null) {
                            startBP(ProccessBP.formTitle, execution, period, group, null, null);
                            for (StructureOrgan structureOrgan : aStructureOrgan) {
                                SubjectReport_StructureOrgan subjectReport_StructureOrgan
                                        = builderSubjectReport.createAndStoreInstance(structureOrgan, period, positions, categories);
                                
                                if (subjectReport_StructureOrgan != null) {
                                    LOG.info("Save sso - " + subjectReport_StructureOrgan.getId());
                                    // сохранение истории в табличку запуска бп для всех бизнес-процесов(3 шт)!!!!!!!!!!!!!!!!!!!!!
                                    LanchBP lanchBP = new LanchBP();
                                    lanchBP.setoSubjectReport_StructureOrgan(subjectReport_StructureOrgan);
                                    lanchBP.setoStatusBP(statusBP_beforeSendForm);
                                    lanchBP.setoDateStart(new DateTime(new Date()));
                                    lanchBP = lanchBpDao.saveOrUpdate(lanchBP);
                                    if (structureOrgan != null) {
                                        for (ProccessBP proccessBP : ProccessBP.values()) {
                                            if (ProccessBP.formTitle != proccessBP) {
                                                startBP(proccessBP, execution, period, group, subjectReport_StructureOrgan, structureOrgan.getoStructure());
                                            }
                                        }
                                    }
                                    lanchBP.setoStatusBP(statusBP_afterSendForm);
                                    lanchBpDao.saveOrUpdate(lanchBP);
                                }
                            }
                        //}
                    }
                }
            }
        }
    }

    private void startBP(ProccessBP proccessBP, DelegateExecution execution, Period period,
            Group group, SubjectReport_StructureOrgan subjectReport_StructureOrgan, Structure structure) {
        Map<String, Object> variableMap = new HashMap<String, Object>();
        variableMap.put(Constant.organ_ID, group.getId());
        variableMap.put(Constant.organName, group.getName());
        variableMap.put(Constant.nYear, execution.getVariable(Constant.nYear));
        variableMap.put(Constant.sYear, String.valueOf(execution.getVariable(Constant.nYear)));
        variableMap.put(Constant.nQuater, execution.getVariable(Constant.nQuater));
        variableMap.put(Constant.sQuater, Constant.mQuaters.get(execution.getVariable(Constant.nQuater)));
        variableMap.put(Constant.oDateFinish_Period, period.getoDateFinish().toLocalDateTime().toDate());
        variableMap.put(Constant.nID_Structure, structure != null ? structure.getId() : null);
        variableMap.put(Constant.sStructureName, structure != null ? structure.getName() : "");
        if (subjectReport_StructureOrgan != null) {
            variableMap.put(Constant.nID_SubjectReport_SructureOrgan, subjectReport_StructureOrgan.getId());
        }
        identityService.setAuthenticatedUserId(Constant.sAdminUser); // убрать
        //System.out.println("Run!! - "+variableMap.toString());
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByKey(proccessBP.name(), variableMap);
        if (processInstance == null || processInstance.getId() == null) {
            throw new IllegalArgumentException(String.format(
                    "process was not started by key:{%s}", proccessBP.name()));
        }
    }

}
