package org.igov.staff.dao;

import org.igov.staff.model.Organ;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface OrganDao extends EntityDao<Organ> {
    
    public Organ getOrgan(String sName);
    
}
