package org.igov.staff.rest.controller;

import com.google.common.base.Optional;
import java.util.ArrayList;
import org.igov.staff.dao.OrganDao;
import org.igov.staff.model.Organ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.igov.staff.activiti.Util;

/**
 * Created by Sergey_PC on 28.11.2015.
 */
@Controller
@RequestMapping(value = "/staff", produces = {MediaType.APPLICATION_JSON_VALUE})
public class OrganController {

    @Autowired
    private OrganDao organDao;

    @Autowired
    private IdentityService identityService;

    @RequestMapping(value = "/getOrgans", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Organ> getAOrgans() {
        return organDao.findAll();
    }

    @RequestMapping(value = "/getGroups", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Group> getGroups() {
        List<Group> groups = identityService.createGroupQuery().orderByGroupName().asc().list();
        List<Group> result = new ArrayList<Group>();
        for (Group group : groups) {
            if (!Util.isItAdmin(group.getId())) {
                result.add(group);
            }
        }
        return result;
    }

    @RequestMapping(value = "/getOrganByUser", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    Organ getOrganByUser(@RequestParam String userId) {
        Organ organ = null;
        List<Group> groups = identityService.createGroupQuery().groupMember(userId).orderByGroupName().asc().list();
        Group group = (groups != null && groups.size() > 0) ? groups.get(0) : null;
        if (group != null) {
            Optional<Organ> organOpt = organDao.findBy("name", group.getId());
            if (organOpt.isPresent()) {
                organ = organOpt.get();
                organ.setsRight(group.getType());
            }
        }
        return organ;
    }
}
