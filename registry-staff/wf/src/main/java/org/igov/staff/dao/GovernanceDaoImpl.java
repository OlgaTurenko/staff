package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.Governance;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
@Repository
public class GovernanceDaoImpl extends GenericEntityDao<Governance> implements GovernanceDao {

    private static final Logger log = Logger.getLogger(GovernanceDaoImpl.class);

    protected GovernanceDaoImpl() {
        super(Governance.class);
    }

}
