package org.igov.staff.core;

/**
 * Created by Sergey_PC on 24.11.2015.
 */
public class StringUtil {

    public static boolean isEmpty(String str){
        return str == null||str.trim().isEmpty();
    }

    public static String trimStrAndSetDivIfNeed(String value,String divBefore, String divAfter){
        StringBuilder sb = new StringBuilder();
        if(!isEmpty(value)){
            sb.append(divBefore).append(value).append(divAfter);
        }
        return sb.toString();
    }

}
