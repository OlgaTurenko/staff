package org.igov.staff.activiti;

import java.util.HashMap;
import java.util.Map;

public class Constant {
    public static final String sAdminUser = "kermit";
    
    public static final String sIndex = "subjectOrgan.index";
    public static final String sRegion = "subjectOrgan.region";
    public static final String sCity = "subjectOrgan.city";
    public static final String sDistrict = "subjectOrgan.district";
    public static final String sStreet = "subjectOrgan.street";
    public static final String sBuild = "subjectOrgan.build";
    public static final String sPhone = "phone";
    public static final String sFax = "subjectOrgan.fax";
    public static final String sEmail = "email";
    
    public static final String organ_ID = "act_id_group.id_";
    public static final String organName = "act_id_group.name_";
    public static final String sLast = "act_id_user.last";
    public static final String sFirst = "act_id_user.first";
    public static final String sMiddle = "act_id_user.middle"; //
    public static final String sLogin = "act_id_user.login";
    public static final String sPassword = "act_id_user.password";
    public static final String sPassword2 = "act_id_user.password2";
    
    public static final String nYear = "period.nYear";
    public static final String sYear = "sYear";
    public static final String nQuater = "period.nQuater";
    public static final String sQuater = "sQuater";
    public static final String oDateStart_Period = "period.oDateStart";
    public static final String oDateFinish_Period = "period.oDateFinish";
    
    public static final String nID_SubjectReport_SructureOrgan = "lanchBP.nID_SubjectReport_SructureOrgan";
    
    public static final String nID_Structure = "structure.nID";
    public static final String sStructureName = "structure.sName";
    public static final String nID_Positiont  = "nID_Positiont";
    public static final String nID_Category   = "nID_Category";
    public static final String nCountOrgan    = "nCountOrgan";
    public static final String sDecision = "decision";

    public static Map<String, String> mQuaters = new HashMap<>();
    static{
        mQuaters.put("1", "I"  );
        mQuaters.put("2", "II" );
        mQuaters.put("3", "III");
        mQuaters.put("4", "IV" );
    }
    
}
