package org.igov.staff.constant;

public enum Quater {
    I  (1), //
    II (2), //
    III(3), //
    IV (4);

    Integer nID;

    private Quater(Integer nID) {
        this.nID = nID;
    }
    
    public Integer getn_ID(){
        return nID;
    }
}
