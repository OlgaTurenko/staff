package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.Category;
import org.springframework.stereotype.Repository;
import org.igov.model.core.EntityDao;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
@Repository
public class CategoryDaoImpl extends GenericEntityDao<Category> implements CategoryDao {

    private static final Logger log = Logger.getLogger(CategoryDaoImpl.class);

    protected CategoryDaoImpl() {
        super(Category.class);
    }

}
